from .base import BaseTranslationService


class MockTranslationService(BaseTranslationService):

    _translations = {
        "it": "Italiano",
        "pl": "Polski",
        "de": "Deutsch",
        "fr": "Français",
        "en": "English",
    }

    def translate(self, message: str, source: str = None, target: str = None):
        if source not in self._translations.keys():
            raise Exception("Unsuppoted source language")
        
        if target is not None and target in self._translations.keys():
            return { target: _translations[target] }
        elif target == 'all' or target is None:
            return self._translations
        else:
            raise Exception("Unsupported target language")
                                            
                                            
            
            
