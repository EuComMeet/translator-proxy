import argostranslate.package
import argostranslate.translate
import itertools

from .base import BaseTranslationService


class ArgosTranslationService(BaseTranslationService):

    def __init__(self):
        installed_packages = argostranslate.package.get_installed_packages()
        self.__supported_languages = list(set(itertools.chain(*[ [p.from_code, p.to_code] for p in installed_packages])))
    
    def translate(self, message: str, source: str = None, target: str = None):
        if source is None:
            source = 'en'

        if source not in self.__supported_languages:
            raise Exception("Unsuppoted source language")
        
        if target is not None and target in self.__supported_languages:
            return { target: argostranslate.translate.translate(message, source, target) }
        elif target == 'all' or target is None:
            return dict([ (t, argostranslate.translate.translate(message, source, t)) for t in self.__supported_languages if t != source ])
        else:
            raise Exception("Unsupported target language")
                                            
                                            
