import abc

class BaseTranslationService(abc.ABC):

    def translate(self, message: str, source: str = None, target: str = None):
        pass

    
