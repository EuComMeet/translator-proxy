from .base import BaseTranslationService
import requests
import os

class DcuTranslationService(BaseTranslationService):

    _supported_languages = ['en', 'it', 'pl', 'de', 'fr']
    
    def __init__(self):
        login_data = {
            "username": os.environ.get("DCU_USERNAME"),
            "password": os.environ.get("DCU_PASSWORD")
        }
        self.api_url = os.environ.get("DCU_API_URL", 'https://mt.computing.dcu.ie')
        self.mt_endpoint = f"{self.api_url}/translate"
        try:
            print(f"Loggin to DCU server {self.api_url}...")
            res = requests.post(f"{self.api_url}/login", json=login_data)
            if 'token' in res:
                self.jwt = res['token']
            else:
                if 'error' in res:
                    raise Exception(res['error'])
                else:
                    raise Exception("No token and no error in response")
        except Exception as e:
            print(e)
            print("Failed to initialize Machine translation service")
            exit(-1)

    def translate(self, message: str, source: str = None, target: str = None):
        if source is None:
            source = 'en' # TODO: autodetect
        if source not in self._supported_languages:
            raise Exception("Unsupported source language")
        if target not in self._supported_languages or target != 'all':
            raise Exception("Unsupported target language")
        res = requests.post(self.mt_endpoint,
                            headers={
                                'Authorization': f"Bearer {jwt_token}"
                            },
                            json=trans_request)
        trans_res = res.json()
        if 'state' in trans_res and trans_res["state"] == 'OK':
            return trans_res["result"]


        
        
        


    

            
        
        
