from .dcu import DcuTranslationService
from .mock import MockTranslationService
from .argos import ArgosTranslationService

class TranslationService:

    @staticmethod
    def from_string(name: str):
        services = {
            'dcu': DcuTranslationService,
            'mock': MockTranslationService,
            'argos': ArgosTranslationService,
        }
        if name in services.keys():
            return services[name]()
        else:
            raise Exception(f"Translation service {name} unknown please choose from {services.keys()}")
        
