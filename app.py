#!/usr/bin/env python3
import json
import os
from flask import Flask
from flask import request
import sqlalchemy as db
from nextcloud import Nextcloud
from modules.services import TranslationService

translation_service = TranslationService.from_string(os.environ.get("TRANSLATION_SERVICE"))

db_uri = os.environ.get("DATABASE_URI")
engine = db.create_engine(db_uri)
nextcloud = Nextcloud(db=engine)

app = Flask(__name__)

@app.route('/', methods=['GET'])
def index():
    return translation_service.translate("Ciao sto funzionando", source='it', target='all'), 200

@app.route('/handleMessage', methods=['POST'])
def handleMessage():
    message = request.form.get("msg")    
    print(request.form)
    actor_id = request.form.get('actor_id')
    message_id = request.form.get('msgId')
    src_lang =  request.form.get('lang')
    if not src_lang:
        src_lang = nextcloud.get_user_language(actor_id)
    try:
        trans = translation_service.translate(message, source=src_lang, target='all')
        nextcloud.add_translations(actor_id, message_id, trans)
    except Exception as e:
        print(e)
        print("Failed to translate!")
    return trans, 200

