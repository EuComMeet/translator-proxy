from sqlalchemy import text
import json

class Nextcloud:
    def __init__(self, db):
        self.db = db
        self.users = self.get_user_langs_map()
        
    def get_user_langs_map(self):
        query = text("SELECT userid AS user, configvalue AS lang FROM oc_preferences WHERE  appid = 'core' and configkey = 'locale'")
        return dict(self.db.execute(query).fetchall())

    def update_user_langs_map(self):
        self.users = self.get_user_langs_map()

    def get_user_language(self, userid):
        return self.users[userid] if userid in self.users else 'en'

    def add_translations(self, actor_id, message_id, translations):
        query = text(f"INSERT INTO oc_comments (parent_id, topmost_parent_id, actor_type, actor_id, message, verb, creation_timestamp, object_id) VALUES (:message_id, :message_id, 'users', :actor_id, :message, 'translations', NOW(), 2)")
        self.db.execute(query, { 'message_id': message_id, 'actor_id': actor_id, 'message': json.dumps(translations)})
        #self.db.execute(query, { 'message_id': message_id, 'actor_id': actor_id, 'message': 're'})
