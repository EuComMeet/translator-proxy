# 🐟 TranslatorProxy 

A modular Machine Translation proxy service built for EuComMeet project

## 🚀 Install 

Here we'll cover installation in a virtual environment with argos as translation provider and setup of a systemd service.

```
git clone https://codeberg.org/EuComMeet/translator-proxy
cd translator-proxy
python3 -m venv venv
. ./venv/bin/activate
pip install wheel # ensure wheel it's installed
pip install -r requirements.txt
bash scripts/install-argos-packages.sh
```

Create a `.env` file with the following content, adjust DATABASE_URI to match your nextcloud db.

```
TRANSLATION_SERVICE=argos
DATABASE_URI=mysql+pymysql://user:pass@localhost/nextcloud?charset=utf8mb4
```

Customize the translatorproxy.service file changing WorkingDirectory and ExecStart path (also host and port if needed)
and copy to `/etc/systemd/system`.

Start the service with `systemctl start translatorproxy`.

Check everything is running with: `curl http://127.0.0.1:1222`, you should see the following content:

```
{"de":"Hi ich arbeite","en":"Hi I'm working","fr":"Bonjour, je travaille.","pl":"Cze\u015b\u0107, pracuj\u0119."}
```

## 🛠️  Configuration 

Translator proxy it's configured using an env file. A different translation service can be choosen changin the `TRANSLATION_SERVICE` var.

The following services are available:
- *argos* Uses [Argos Translate](https://www.argosopentech.com/). Argos packages must be installed, make sure to run `scripts/install-argos-packages.sh`
- *dcu* Used api provided by DCU for the EuComMeet project. `DCU_USERNAME` and `DCU_PASSWORD` must be set, `DCU_API_URL` can be used to override 
default endpoint (https://mt.computing.dcu.ie)
- *moc* Fastest translation model in the world, does nothing, just return dummy translations for development use.

# 💼 License

GPLv3 see `LICENSE.md`
